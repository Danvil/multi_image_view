#include "WdgtTabView.h"
#include "ui_WdgtTabView.h"
#include <QtGui/QMdiSubWindow>
#include <QtGui/QLabel>

namespace miv
{

WdgtTabView::WdgtTabView(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui_WdgtTabViewClass();
	ui->setupUi(this);

	miv_.reset(new MultiImageView());

	QObject::connect(&timer_, SIGNAL(timeout()), this, SLOT(tick()));
	timer_.setInterval(40);
	timer_.start();
}

WdgtTabView::~WdgtTabView()
{
	delete ui;
}

void WdgtTabView::addCustomWidget(QWidget* w, const QString& title)
{
	ui->tabs->addTab(w, title);
}

void WdgtTabView::tick()
{
	std::set<std::string> dirty_tags = miv_->collectDirtyTags();
	for(const std::string& tag : dirty_tags) {
		showImage(tag, miv_->get(tag));
	}
}

void WdgtTabView::showImage(const std::string& tag, const QImage& qimg)
{
	// std::map<std::string, bool> tabs_usage;
	std::map<std::string, QWidget*> tabs_labels;
	// prepare tabs
	for(int i=0; i<ui->tabs->count(); i++) {
		std::string text = ui->tabs->tabText(i).toStdString();
		// tabs_usage[text] = (text == "3D");
		tabs_labels[text] = ui->tabs->widget(i);
	}
	// add tabs if necessary and display images
	// find label and create new if none exists
	QLabel* qlabel;
	if(tabs_labels.find(tag) != tabs_labels.end()) {
		qlabel = (QLabel*)tabs_labels[tag];
	} else {
		qlabel = new QLabel();
		tabs_labels[tag] = qlabel;
		ui->tabs->addTab(qlabel, QString::fromStdString(tag));
	}
	// tabs_usage[text] = true;
	// display image in label
	qlabel->setPixmap(QPixmap::fromImage(qimg));
	// // delete unused tabs
	// for(auto p : tabs_usage) {
	// 	if(!p.second) {
	// 		for(int i=0; i<ui->tabs->count(); i++) {
	// 			if(ui->tabs->tabText(i).toStdString() == p.first) {
	// 				ui->tabs->removeTab(i);
	// 			}
	// 		}
	// 	}
	// }
}

}
