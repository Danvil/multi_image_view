#ifndef MIV_WDGTTABVIEW_HPP
#define MIV_WDGTTABVIEW_HPP

#include "MultiImageView.hpp"
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include <memory>

class Ui_WdgtTabViewClass;

namespace miv
{

	class WdgtTabView : public QWidget
	{
	    Q_OBJECT

	public:
		WdgtTabView(QWidget *parent = 0);
		~WdgtTabView();

		std::shared_ptr<MultiImageView> miv() const { return miv_; }

		void addCustomWidget(QWidget* w, const QString& title);

	protected Q_SLOTS:
		void tick();

	private:
		void showImage(const std::string& tag, const QImage& qimg);

	private:
		Ui_WdgtTabViewClass* ui;
		QTimer timer_;
		std::shared_ptr<MultiImageView> miv_;

	};

}

#endif
