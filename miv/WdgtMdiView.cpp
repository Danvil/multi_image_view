#include "WdgtMdiView.h"
#include "ui_WdgtMdiView.h"
#include <QtGui/QMdiSubWindow>
#include <QtGui/QLabel>

namespace miv
{

WdgtMdiView::WdgtMdiView(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui_WdgtMdiViewClass();
	ui->setupUi(this);

	miv_.reset(new MultiImageView());

	QObject::connect(&timer_, SIGNAL(timeout()), this, SLOT(tick()));
	timer_.setInterval(40);
	timer_.start();

	// QObject::connect(ui->actionTile_Subwindows, SIGNAL(triggered()), ui->mdiArea, SLOT(tileSubWindows()));
	// QObject::connect(ui->actionCascade_Subwindows, SIGNAL(triggered()), ui->mdiArea, SLOT(cascadeSubWindows()));

}

WdgtMdiView::~WdgtMdiView()
{
}

void WdgtMdiView::addCustomWidget(QWidget* w, const QString& title)
{
	auto sw = ui->mdiArea->addSubWindow(w);
	sw->setWindowTitle(title);
}

void WdgtMdiView::tick()
{
	std::set<std::string> dirty_tags = miv_->collectDirtyTags();
	for(const std::string& tag : dirty_tags) {
		showImage(tag, miv_->get(tag));
	}
}

void WdgtMdiView::showImage(const std::string& tag, const QImage& qimg)
{
	// prepare subwindow list
	std::map<std::string, QMdiSubWindow*> subwindows_by_tag;
	for(QMdiSubWindow* w : ui->mdiArea->subWindowList()) {
		subwindows_by_tag[w->windowTitle().toStdString()] = w;
	}
	// find label
	auto it = subwindows_by_tag.find(tag);
	// create new if none exists
	QMdiSubWindow* sw;
	QLabel* qlabel;
	if(it != subwindows_by_tag.end()) {
		sw = it->second;
		qlabel = (QLabel*)sw->widget();
	} else {
		qlabel = new QLabel();
		subwindows_by_tag[tag] = sw;
		sw = ui->mdiArea->addSubWindow(qlabel);
		sw->setWindowTitle(QString::fromStdString(tag));
		sw->show();
	}
	// display image in label
	qlabel->setPixmap(QPixmap::fromImage(qimg));
	// qlabel->setPixmap(QPixmap::fromImage(qimg->copy(30, 25, 640-65, 480-45)));
	qlabel->adjustSize();
	sw->adjustSize();
}

}
