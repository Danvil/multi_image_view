#include "WdgtFourView.h"
#include "ui_WdgtFourView.h"

namespace miv
{

WdgtFourView::WdgtFourView(QWidget *parent)
: QWidget(parent)
{
	ui = new Ui_WdgtFourViewClass();
	ui->setupUi(this);

	miv_.reset(new MultiImageView());

	QObject::connect(&timer_, SIGNAL(timeout()), this, SLOT(tick()));
	timer_.setInterval(40);
	timer_.start();
}

WdgtFourView::~WdgtFourView()
{
	delete ui;
}

void WdgtFourView::addCustomWidget(QWidget* w, const QString& title)
{

}

void WdgtFourView::tick()
{
	std::set<std::string> dirty_tags = miv_->collectDirtyTags();
	for(const std::string& tag : dirty_tags) {
		showImage(tag, miv_->get(tag));
	}
}

void WdgtFourView::showImage(const std::string& tag, const QImage& qimg)
{
	// FIXME implement
}

}