#include "WdgtImage.hpp"

WdgtImage::WdgtImage(QWidget *parent)
{
	this->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
	    this, SLOT(ShowContextMenu(const QPoint&)));
}

WdgtImage::~WdgtImage()
{

}

void WdgtImage::ShowContextMenu(const QPoint& pos)
{
    // for most widgets
    QPoint globalPos = this->mapToGlobal(pos);
    // for QAbstractScrollArea and derived classes you would use:
    // QPoint globalPos = myWidget->viewport()->mapToGlobal(pos); 

    QMenu myMenu;
    myMenu.addAction("Menu Item 1");
    // ...

    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem)
    {
        ui.label->setText(selectedItem->text());
    }
    else
    {
        // nothing was chosen
    }
}

