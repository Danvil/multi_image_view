#ifndef INCLUDED_MIV_MULTIIMAGEVIEW_HPP
#define INCLUDED_MIV_MULTIIMAGEVIEW_HPP

#include <QtGui/QImage>
#include <mutex>
#include <map>
#include <set>
#include <string>

namespace miv
{

	class MultiImageView
	{
	public:
		std::set<std::string> tags() const {
			std::lock_guard<std::mutex> lock(mutex_);
			return tags_;
		}

		std::set<std::string> collectDirtyTags() {
			std::lock_guard<std::mutex> lock(mutex_);
			std::set<std::string> t = dirty_;
			dirty_.clear();
			return t;
		}

		void set(const std::string& tag, const QImage& img) {
			std::lock_guard<std::mutex> lock(mutex_);
			images_[tag] = img;
			tags_.insert(tag);
			dirty_.insert(tag);
		}

		QImage get(const std::string& tag) const {
			std::lock_guard<std::mutex> lock(mutex_);
			auto it = images_.find(tag);
			if(it == images_.end()) {
				// not found 
				return QImage();
			}
			else {
				return it->second;
			}
		}

	private:
		mutable std::mutex mutex_;
		std::map<std::string, QImage> images_;
		std::set<std::string> tags_;
		std::set<std::string> dirty_;
	};
}

#endif
