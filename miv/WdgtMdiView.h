#ifndef MIV_WDGTMDIVIEW_HPP
#define MIV_WDGTMDIVIEW_HPP

#include "MultiImageView.hpp"
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include <memory>

class Ui_WdgtMdiViewClass;

namespace miv
{

	class WdgtMdiView : public QWidget
	{
	    Q_OBJECT

	public:
		WdgtMdiView(QWidget *parent = 0);
		~WdgtMdiView();

		std::shared_ptr<MultiImageView> miv() const { return miv_; }

		void addCustomWidget(QWidget* w, const QString& title);

	protected Q_SLOTS:
		void tick();

	private:
		void showImage(const std::string& tag, const QImage& qimg);

	private:
		Ui_WdgtMdiViewClass* ui;
		QTimer timer_;
		std::shared_ptr<MultiImageView> miv_;

	};

}

#endif
