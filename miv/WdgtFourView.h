#ifndef MIV_WDGTFOURVIEW_HPP
#define MIV_WDGTFOURVIEW_HPP

#include "MultiImageView.hpp"
#include <QtGui/QWidget>
#include <QtCore/QTimer>
#include <memory>

class Ui_WdgtFourViewClass;

namespace miv
{

	class WdgtFourView : public QWidget
	{
	    Q_OBJECT

	public:
		WdgtFourView(QWidget *parent = 0);
		~WdgtFourView();

		std::shared_ptr<MultiImageView> miv() const { return miv_; }

		void addCustomWidget(QWidget* w, const QString& title);

	protected Q_SLOTS:
		void tick();

	private:
		void showImage(const std::string& tag, const QImage& qimg);

	private:
		Ui_WdgtFourViewClass* ui;
		QTimer timer_;
		std::shared_ptr<MultiImageView> miv_;

	};

}
#endif
