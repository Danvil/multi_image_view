#ifndef DASPSTRANDS_WDGTIMAGE_H
#define DASPSTRANDS_WDGTIMAGE_H

#include "ui_WdgtImage.h"
#include <QtGui/QWidget>

class WdgtImage : public QWidget
{
    Q_OBJECT

public:
	WdgtImage(QWidget *parent = 0);
	~WdgtImage();

public Q_SLOTS:
	void ShowContextMenu(const QPoint& pos);

private:

private:
    Ui::WdgtImageClass ui;
};

#endif
